import React from 'react';   
import PropTypes from 'prop-types'; 
import * as sessionActions from '../../actions/sessionActions';
import { connect } from 'react-redux';
import { bindActionCreators} from 'redux';
import {LOGIN_FORM_STATUS} from '../../utils/consts'; 
import { 
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TextInput,
  Button,
  TouchableOpacity,
  ImageBackground,
  Alert
} from 'react-native';
 
const { width, height } = Dimensions.get("window");
const background = require("./login1_bg.png");
const mark = require("./login1_mark.png");
const lockIcon = require("./login1_lock.png");
const personIcon = require("./login1_person.png");
 

class signInScreen extends React.Component { 
  constructor(props, context){
    super(props, context); 
    this.handleChanged = this.handleChanged.bind(this);
    this.login = this.login.bind(this);
    this.state = {
        userName:'',
        password:'' ,
        
    };
 } 

 handleChanged(name, value){ 
  let newState = {...this.state};
  newState[name] = value; 
  this.setState(newState);  
}

  render() {
    let disabled = this.props.loginFormStatus == LOGIN_FORM_STATUS.LOGINING || this.state.userName == '' || this.state.password == '';
    return (

      <View style={styles.container}>
        <ImageBackground source={background} style={styles.background} resizeMode="cover">
          <View style={styles.markWrap}>
            <Image source={mark} style={styles.mark} resizeMode="contain" />
          </View>
          <View style={styles.wrapper}>
            <View style={styles.inputWrap}>
              <View style={styles.iconWrap}>
                <Image source={personIcon} style={styles.icon} resizeMode="contain" />
              </View>
              <TextInput 
                placeholder="Username" 
                placeholderTextColor="#FFF"
                style={styles.input} 
                onChangeText={(userName) => this.handleChanged("userName", userName)} 
                name="userName"
              />
            </View>
            <View style={styles.inputWrap}>
              <View style={styles.iconWrap}>
                <Image source={lockIcon} style={styles.icon} resizeMode="contain" />
              </View>
              <TextInput 
                placeholderTextColor="#FFF"
                placeholder="Password" 
                style={styles.input} 
                secureTextEntry={true} 
                onChangeText={(password) => this.handleChanged("password", password)} 
                name="password"
              />
            </View>
            <TouchableOpacity activeOpacity={.5}>
              <View style={styles.infoWrap}>
                <Text style={styles.errorText} >{this.props.loginFormStatus == LOGIN_FORM_STATUS.LOGINFAILED ? "Invalid user/password" : null}</Text>
                <Text style={styles.forgotPasswordText}>Forgot Password?</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={.5} onPress={this.handleLogin} disabled={disabled} onPress={this.login}>
              <View style={[styles.button, {backgroundColor: disabled ? "silver" : "#FF3366"}]}>
                <Text style={styles.buttonText} >Sign In</Text>
              </View>
            </TouchableOpacity> 
            
          </View>
          <View style={styles.container}>
            <View style={styles.signupWrap}>
              <Text style={styles.accountText}>Don't have an account?</Text>
              <TouchableOpacity activeOpacity={.5} >
                <View>
                  <Text style={styles.signupLinkText}>Sign Up</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </ImageBackground>
      </View>
 
    );
  }

  login() {
    this.props.actions.login(this.state.userName, this.state.password, this.props.navigation);
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  markWrap: {
    flex: 1,
    paddingVertical: 30,
  },
  mark: {
    width: null,
    height: null,
    flex: 1,
  },
  background: {
    width,
    height,
  },
  wrapper: {
    paddingVertical: 30,
  },
  inputWrap: {
    flexDirection: "row",
    marginVertical: 10,
    height: 40,
    borderBottomWidth: 1,
    borderBottomColor: "#CCC"
  },
  iconWrap: {
    paddingHorizontal: 7,
    alignItems: "center",
    justifyContent: "center",
  },
  icon: {
    height: 20,
    width: 20,
  },
  input: {
    flex: 1,
    paddingHorizontal: 10,
  },
  button: {
    backgroundColor: "#FF3366",
    paddingVertical: 20,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
  },
  buttonText: {
    color: "#FFF",
    fontSize: 18,
  },  
  infoWrap: {
    flexDirection: "row",
    marginVertical: 2,
    height: 30
  },
  forgotPasswordText: {
    color: "#D8D8D8",
    backgroundColor: "transparent",
    textAlign: "right",
    paddingRight: 15,
    flex:1
  },

  errorText: {
    color: "red",
    backgroundColor: "transparent",
    textAlign: "left",
    paddingLeft: 15,
  },
  signupWrap: {
    backgroundColor: "transparent",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  accountText: {
    color: "#D8D8D8"
  },
  signupLinkText: {
    color: "#FFF",
    marginLeft: 5,
  }
});



function mapStateToProps(state, ownProps) {
  return {
      loginFormStatus: state.loginFormStatus 
  };
}

function mapDispatchToProps(dispatch) {
 return {
     actions: bindActionCreators(sessionActions, dispatch)
 };
}


export default connect(mapStateToProps, mapDispatchToProps)(signInScreen); 
