import React from 'react';
import {
  ActivityIndicator, 
  Button,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators} from 'redux'; 
import AsyncStorage from '@react-native-community/async-storage';
import * as positionActions from '../actions/coinPositionActions';

class coinPositionScreen extends React.Component {
  static navigationOptions = {
    title: "Coin Positions"
  };
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "powderblue" }}>
        <Text>ABC</Text>
      </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
      accounts: state.accounts,
      positions: state.positions,
      refreshTime: state.refreshTime,
      loading: state.loading,
      error: state.error
  };
}

function mapDispatchToProps(dispatch) {
 return {
     actions: bindActionCreators(positionActions, dispatch)
 };
}


export default connect(mapStateToProps, mapDispatchToProps)(coinPositionScreen);