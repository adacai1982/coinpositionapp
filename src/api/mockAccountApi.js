import delay from './delay';

const accounts = [
    {
        id:1,
        name: 'Binance-Triag',
        selected: true
    },
    {
        id:2, 
        name: 'Bitmax-Triag',
        selected: true
    },
    {
        id:3,
        name: 'Bitfinex-Triag',
        selected: true
    },
    {
        id:4,
        name: 'Bitstamp-Triag',
        selected: true
    },
    {
        id:5,
        name: 'Gdax-Triag' ,
        selected: true
    }
];

class AccountApi {
    static getAccounts() {
        return new Promise((resolve, reject) => {
          setTimeout(() => {
            resolve(Object.assign([], accounts));
          }, delay);
        });
      }
}

export default AccountApi;