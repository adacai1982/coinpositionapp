import * as types from './actionTypes';
import coinPositionApi from '../api/mockCoinPositionApi'; 


let lastPositions = [];
export function loadCoinPositionsSuccess(positions){
    return {type:types.LOAD_COIN_POSITIONS_SUCCESS , positions};
}
 
export function loadingCoinPositions(){
    return {type: types.LOADING_COIN_POSITIONS};
}

export function failedToLoadSomeCoinPositions(missingAccounts){
    return {type:types.FAILED_TO_LOAD_SOME_COIN_POSITIONS, missingAccounts};
}
export function loadCoinPositions(accounts) {

    return function(dispatch){ 
        dispatch(loadingCoinPositions());
        return coinPositionApi.getCoinPositions(accounts).then(positions=>{ 
            let missingAccounts = accounts.filter(a=> a.selected && !positions.some(p=>p.accountName == a.name));
            if (missingAccounts.length > 0){  
                let noneAccounts;
                if (lastPositions.length > 0){
                    let stalePositions = lastPositions.filter(p=> missingAccounts.some(a=>a.name == p.accountName)).map(p=>{
                        return Object.assign({}, p, {
                            isError: false,
                            isWarning: true
                        });
                    }); 
                   
                    lastPositions = [...positions];
                    if (stalePositions && stalePositions.length > 0){ 
                        positions.push(...stalePositions);
                        lastPositions.push(...stalePositions);
                        noneAccounts = missingAccounts.filter(a=> !stalePositions.some(p=>p.accountName == a.name));
                    }
                    else{
                        noneAccounts = missingAccounts;
                    }
                }
                else{ 
                    lastPositions = [...positions];
                    noneAccounts = missingAccounts;
                }
                positions.push(...noneAccounts.map(a=>{
                    return {
                        accountName: a.name, 
                        isError: true, 
                        isWarning: false
                    };
                }));
                
            }
            else{
                lastPositions = [...positions];
            }
            
                
            dispatch(loadCoinPositionsSuccess(positions));
            if (missingAccounts.length > 0) { 
                dispatch(failedToLoadSomeCoinPositions(missingAccounts)); 
            }
        }).catch(error => {
            throw(error);
        });
    };
}

export function toggleAccount(account, selected){
    return { type:types.TOGGLE_ACCOUNT, account, selected };
}

 