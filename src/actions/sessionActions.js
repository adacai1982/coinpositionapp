import * as types from './actionTypes';
import sessionApi from '../api/mockSessionApi';  
import {LOGIN_FORM_STATUS} from '../utils/consts'; 
import { NavigationActions } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage'; 
import {Alert} from 'react-native';
export function updateLoginStatus(status){
   
    return {type:types.UPDATE_LOGIN_STATUS, status};
}
   
export function login(userName, password, navigation) {

    return function(dispatch){ 

        dispatch(updateLoginStatus(LOGIN_FORM_STATUS.LOGINING));
        return sessionApi.login(userName, password).then(success=>{ 
            dispatch(updateLoginStatus(success ? LOGIN_FORM_STATUS.LOGINED: LOGIN_FORM_STATUS.LOGINFAILED)); 

            if (success){
                  navigation.navigate('App'); 
                  AsyncStorage.setItem('userToken', userName).then(()=>{
                      console.log("userToken saved");
                  }); 
            } 

        }).catch(error => {
            throw(error);
        });
    };
}

 export function logout(){ 
 }
 
