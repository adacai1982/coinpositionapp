import * as types from './actionTypes';
import accountApi from '../api/mockAccountApi'; 
import {loadCoinPositions} from './coinPositionActions';


export function loadAccountsSuccess(accounts){
   
    return {type:types.LOAD_ACCOUNTS_SUCCESS, accounts};
}
  
export function loadAccounts(loadPositions) {

    return function(dispatch){ 
        return accountApi.getAccounts().then(accounts=>{
            dispatch(loadAccountsSuccess(accounts, loadPositions));
            if (loadPositions){
                loadCoinPositions(accounts)(dispatch);
            }
        }).catch(error => {
            throw(error);
        });
    };
}

