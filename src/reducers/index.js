import { combineReducers} from 'redux';
import positions from './coinPositionReducer'; 
import accounts from './accountReducer'; 
import refreshTime from './refreshTimeReducer'; 
import loading from './loadingReducer'; 
import error from './errorReducer';
import loginFormStatus from './loginFormStatusReducer';   
 
const rootReducer =  combineReducers({ 
        accounts,
        positions,
        refreshTime,
        loading,
        error,
        loginFormStatus 
    });

export default rootReducer;