import * as types from '../actions/actionTypes';
import initialState from './initialState';


export default function coinPositionReducer(state =initialState.error, action){
    switch(action.type){
        case types.FAILED_TO_LOAD_SOME_COIN_POSITIONS:
             if (action.missingAccounts){
                 return "Failed to load position of account " + action.missingAccounts.map(a=>a.name).join(',');
             }
             return ''; 
        default:
            return '';
    }
}