import * as types from '../actions/actionTypes';
import initialState from './initialState';


export default function accountReducer(state =initialState.accounts, action){
    switch(action.type){
        case types.LOAD_ACCOUNTS_SUCCESS:
            return action.accounts;
        case types.TOGGLE_ACCOUNT: 
            {
                let account = action.account;
                let selected = action.selected;
                let foundedIndex = state.indexOf(account);
                if (foundedIndex >= 0){
                    let newAccount = Object.assign({}, account, {selected: selected});

                    if (foundedIndex == 0){
                        return [newAccount, ...state.slice(1)];
                    }
                    if (foundedIndex == state.length - 1){
                        return [...state.slice(0, state.length - 1), newAccount];
                    } 
                    return [...state.slice(0, foundedIndex), newAccount, ...state.slice(foundedIndex + 1)];
                }
                return state; 
            }
        default:
            return state;
    }
}