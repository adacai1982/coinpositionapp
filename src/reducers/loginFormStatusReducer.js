import * as types from '../actions/actionTypes';
import initialState from './initialState';


export default function loginFormStatusReducer(state =initialState.loginFormStatus, action){
    switch(action.type){
        case types.UPDATE_LOGIN_STATUS:
            return action.status;
        default:
            return state;
    }
}