import * as types from '../actions/actionTypes';
import initialState from './initialState';


export default function coinPositionReducer(state =initialState.refreshTime, action){
    switch(action.type){
        case types.LOAD_COIN_POSITIONS_SUCCESS:
            return new Date().toLocaleTimeString();  
        default:
            return state;
    }
}