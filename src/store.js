import { createStore, applyMiddleware } from 'redux';
import AppReducer from './reducers'
import thunkMiddleware from 'redux-thunk';
import promise from 'redux-promise';
import initialState from './reducers/initialState';

export default store = createStore(
  AppReducer,
  initialState,
  applyMiddleware(
    thunkMiddleware,
    promise
  )
); 